# Challenge: Root Cause Analysis with a Python config app

This project implements an application that parses configuration and initializes an SQLite database, which both work well without any dependencies. It uses best practices in CI/CD with a Python environment and caching. The latest feature implementation adds a Redis caching client, and now the CI/CD build is failing for some reason.

Use [GitLab Duo Root Cause Analysis](https://docs.gitlab.com/ee/user/ai_features.html#root-cause-analysis) to explain and fix the build errors.

Everything is allowed - except for opening your browser search. All GitLab Duo features can be used: [Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html), [Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/), etc.

## Tips

1. Add [GitLab Duo Chat as optional help](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#ask-about-cicd), too.
    - For example, root cause analysis suggests to install a specific package.
    - Instead, if you want to use a different image -- ask Chat about it.
1. Fix the missing module dependencies in CI/CD.
1. Run Root Cause Analysis again to learn about missing services. Ask Duo Chat how to start `services` in CI/CD jobs.

## Solution

[solution/](solution/).

## Author

@dnsmichi

## Development

The source code in [src/main.py](src/main.py) was generated with GitLab Duo Code Suggestions, and refactored with GitLab Duo Chat.

On macOS, create a virtual environment first.

```shell
python3 -m venv myenv

source myenv/bin/activate 
```