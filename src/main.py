import configparser
import sqlite3
import redis

def load_config(config_file):
    """Load the configuration from a file."""
    config = configparser.ConfigParser()
    config.read(config_file)
    return config

def get_app_name(config):
    """Get the application name from the configuration."""
    app_name = config.get('application', 'name')
    return app_name

def initialize_database(db_file):
    """Initialize the SQLite database."""
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    cursor.execute('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, name TEXT, email TEXT)')
    cursor.execute('INSERT INTO users (name, email) VALUES (?, ?)', ('John', 'john@example.com'))
    cursor.execute('INSERT INTO users (name, email) VALUES (?, ?)', ('Michi', 'michi@example.com'))
    conn.commit()
    return conn, cursor

def print_users(cursor, redis_client):
    """Print all users from the database or Redis cache."""
    users = redis_client.get('users')
    if users is None:
        cursor.execute('SELECT * FROM users')
        users = cursor.fetchall()
        redis_client.set('users', str(users))
    else:
        users = eval(users)
    print(users)

# Load the configuration from config.ini
config = load_config('config.ini')

# Get the application name from the configuration
app_name = get_app_name(config)
print(f"Application name: {app_name}")

# Initialize the Redis client
redis_client = redis.Redis(host='redis', port=6379, db=0)

# Initialize the SQLite database backend
conn, cursor = initialize_database('database.db')

# Print all users from the database or Redis cache
print_users(cursor, redis_client)

# Close the database connection
conn.close()